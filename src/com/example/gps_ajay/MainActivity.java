package com.example.gps_ajay;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;


public class MainActivity extends Activity {
    
   Button btnShowLocation;
    
   
   GPSTracker gps;
    
   
   public void onCreate(Bundle savedInstanceState) {
       super.onCreate(savedInstanceState);
       setContentView(R.layout.activity_main);
        
       btnShowLocation = (Button) findViewById(R.id.btnShowLocation);
        
       
       btnShowLocation.setOnClickListener(new View.OnClickListener() {
            
           
           public void onClick(View arg0) {        
               
               gps = new GPSTracker(MainActivity.this);

                    
               if(gps.canGetLocation()){
                    
                   double latitude = gps.getLatitude();
                   double longitude = gps.getLongitude();
                    
                   
                   Toast.makeText(getApplicationContext(), "Hi Ajay!!\n Your Location is - \nLat: " + latitude + "\nLong: " + longitude, Toast.LENGTH_LONG).show();    
               }else{
                   // can't get location
                   // GPS or Network is not enabled
                   // Ask user to enable GPS/network in settings
                   gps.showSettingsAlert();
               }
                
           }
       });
   }
    
}